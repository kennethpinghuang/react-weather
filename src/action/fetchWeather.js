import axios from 'axios';
const API_URL =
	'https://api.apixu.com/v1/forecast.json?key=6e4474fdc7e548ce8ba115109190603&days=7&q=';

export const GET_WEATHER = 'GET_WEATHER';
export const SET_LOADING = 'SET_LOADING';

export function fetchWeather(data) {
	return {
		type: GET_WEATHER,
		payload: data
	};
}

export function setLoading(status){
	return {
		type: SET_LOADING,
		payload: status
	};
}

export default function fetchWeatherData(city) {
	const url = `${API_URL}${city}`;
	return (dispatch)=>{axios.get(url)
		.then(response => {
			dispatch(fetchWeather(response.data));
			dispatch(setLoading(false));
		});
	};
}

