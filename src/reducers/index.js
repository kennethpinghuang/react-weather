import {combineReducers} from 'redux';
import getWeather from './getWeather';
import setLoading from './loading';

const reducers = combineReducers({
	weatherData:getWeather,
	loading:setLoading,
});

export default reducers;