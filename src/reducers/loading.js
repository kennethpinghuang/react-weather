import { SET_LOADING } from '../action/fetchWeather';

export default function setLoading(state = true, action) {
	switch (action.type) {
	case SET_LOADING:
		return action.payload;
	default:
		return state;
	}
}