import React, { PureComponent } from 'react';
import CityCondition from './CityCondition';
import Forecaster from './Forecaster';
import { Icon } from 'react-fa';
import fetchWeatherData from './action/fetchWeather';
import { connect } from 'react-redux';

class WeatherChannel extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			curCity: 'Brisbane',
			condition: {
				city: '',
				weather: '',
				temp: '',
				humidity: '',
				wind_gust_mph: '',
				wind_dir: ''
			},
			days: [
			],
			isCelsius: true,
		};
	}

	componentDidMount() {
		this.handleSearch();
	}

	handleCityChange = (e) => {
		const value = e.target.value;
		this.setState({ curCity: value });
	}

	conditionTransfer = (currentData, city) => {
		if (currentData) {
			const condition = {
				city: city,
				temp: { C: currentData.temp_c, F: currentData.temp_f },
				weather: currentData.condition.text,
				humidity: currentData.humidity,
				wind_gust_mph: currentData.wind_mph,
				wind_dir: currentData.wind_dir
			};
			return condition;
		}
	}

	forecastTransfer = (forecastday) => {
		if (forecastday) {
			const forecastDaysWeather = forecastday.map((day, index, array) => {
				const weather = day.day;
				const date = new Date(day.date);
				const icon = `http:${weather.condition.icon}`;
				return {
					weekday_short: weekday[date.getDay()],
					weekday: weekday[date.getDay()],
					high: { celsius: weather.maxtemp_c, fahrenheit: weather.maxtemp_f },
					low: { celsius: weather.mintemp_c, fahrenheit: weather.mintemp_f },
					icon: icon
				};
			});
			return forecastDaysWeather;
		}
	}

	handleSearch = () => {
		const city = this.state.curCity;
		this.props.fetchData(city);
	}

	handleUnitSwitch = () => {
		const isCelsius = !this.state.isCelsius;
		this.setState({ isCelsius });
	}

	render() {
		const { weatherData, loading } = this.props;
		if (loading)
			return (<div>loading</div>)
		else {
			const { isCelsius, curCity } = this.state;
			const { current, forecast } = weatherData;
			return (
				<React.Fragment>
					<nav>
						<div style={{ flex: 1 }}>
							<input className="search-input" value={curCity} onChange={this.handleCityChange} />
							<button className="search-btn" onClick={this.handleSearch}>
								<Icon name="search" />
							</button>
							<button className="temp-switch" onClick={this.handleUnitSwitch}>
								<Icon name={current.temp_c === '' ? 'thermometer-empty' : 'thermometer-full'} />
								{isCelsius ? '  C' : '  F'}
							</button>
						</div>
					</nav>
					<main>
						<section className='weather-condition'>
							<CityCondition data={{ condition: this.conditionTransfer(current, curCity), isCelsius }} />
						</section>
						<section className='weather-forecast'>
							<Forecaster data={{ days: this.forecastTransfer(forecast.forecastday), isCelsius }} />
						</section>
					</main>
				</React.Fragment>
			);
		}

	}
}

const weekday = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
const mapStateToProps = ({ weatherData, loading }) => ({ weatherData, loading });
const mapDispatchToProps = dispatch => {
	return {
		fetchData: (city) => {
			dispatch(fetchWeatherData(city));
		},
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(WeatherChannel);