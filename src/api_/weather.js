import axios from 'axios';
const CONDITION_BASE_URL =
    'http://api.wunderground.com/api/f029e46fd0232d12/geolookup/conditions/q/Australia/';

const FORCAST_BASE_URL =
	'http://api.wunderground.com/api/f029e46fd0232d12/geolookup/forecast10day/q/Australia/';
	
const API_URL = 
	'https://api.apixu.com/v1/forecast.json?key=6e4474fdc7e548ce8ba115109190603&days=7&q='

export function fetchConditionData(city){
	const url= `${CONDITION_BASE_URL}${city}.json`;
	return axios.get(url).then(dataObj=>dataObj.data.current_observation).catch(function (error) {
		// handle error
		console.log(error);
	});
}

export function fetchForecastData(city){
	const url = `${FORCAST_BASE_URL}${city}.json`;
	return axios.get(url).then(dataObj=>dataObj.data.forecast.simpleforecast.forecastday).catch(function (error) {
		// handle error
		console.log(error);
	});
}

export default function fetchWeatherData(city){
	const url = `${API_URL}${city}`;
	return axios.get(url)
				.then(data=>data.data)
}