import React, { Component } from 'react';
import RowForecast from './RowForecast';

export default class Forecaster extends Component {
	constructor(props) {
		super(props);
		this.state = {
			numOfDays: 5
		};
	}

	handleClickOnBtn(e) {
		const numOfDays = (e.target.innerHTML === '5 days') ? 5 : 10;
		this.setState({ numOfDays });
	}

	render() {
		const { data } = this.props;
		return (
			<div>
				<div className="forecast__switch">
					<button className={this.state.numOfDays === 5 ? 'forecast__switch_0 switch-active' : 'forecast__switch_0'} onClick={this.handleClickOnBtn.bind(this)}>
						5 days
					</button>
					<button className={this.state.numOfDays === 5 ? 'forecast__switch_1' : 'forecast__switch_1 switch-active'} onClick={this.handleClickOnBtn.bind(this)}>
						7 days
					</button>
				</div>
				{
					data.days.slice(0, this.state.numOfDays)
						.map((day, index) => {
							return <RowForecast key={`${day.weekday}_${index}`} rowday={{ day, isCelsius: data.isCelsius }} />;
						})
				}
			</div>
		);
	}
}