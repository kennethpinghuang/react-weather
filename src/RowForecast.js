import React from 'react';

export default function RowForecast(props){
	const {day, isCelsius} = props.rowday;
   
	return (
		<div className='weather-forecast__row'>
			<span className='weather-forecast__day'>{day.weekday_short}</span>
			<span className='weather-forecast__icon'>
				<img src={day.icon} alt='day icon'/>
			</span>
			<span className='weather-forecast__high'>{isCelsius?day.high.celsius:day.high.fahrenheit}</span>
			<span className='weather-forecast__low'>{isCelsius?day.low.celsius:day.high.fahrenheit}</span>
		</div>
	);
}