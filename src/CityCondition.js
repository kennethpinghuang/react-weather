import React from 'react';

export default function CityCondition(props) {
	const { city, weather, temp, humidity, wind_gust_mph, wind_dir } = props.data.condition;
	const { isCelsius } = props.data;
	const weatherStyle = { textAlign: 'center', fontsize: 14 };

	return (
		<div>
			<div className='weather-condition__location'>{city}</div>
			<div style={weatherStyle}>{weather}</div>
			<div className='weather-condition__temp'>{isCelsius ? temp.C : temp.F}</div>
			<div className='weather-condition__desc'>
				<div>
					<img src={require('./icon/icon-umberella.png')} alt='icon umberella' />
					<span className='citem'>{humidity}</span>
				</div>
				<div>
					<img src={require('./icon/icon-wind.png')} alt='icon wind' />
					<span className='citem'>{wind_gust_mph}mph</span>
				</div>
				<div>
					<img src={require('./icon/icon-compass.png')} alt='icon compass' />
					<span className='citem'>{wind_dir}</span>
				</div>
			</div>
		</div>
	);
}